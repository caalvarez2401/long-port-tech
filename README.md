# Prueba tecnica Front 2021


## Instalar y correr la aplicación
1. En la carpeta "MARVEL" de la aplicación correr:
   `npm i` o `npm install`


   Toda la aplicación se podrá correr con el comando:
   `npm start`

   

## ¿Como aborde la prueba?
   La prueba la desarrolle principalmente en React con ayudas como boostrap y auth0. La pagina tiene un inicio de sesión, al momento de iniciar, se encontrará con un buscador y debajo de él los personajes de laapi de marvel. Hice uso de hooks como useState, useEffect. Trate de tenerlo ordenada, aunque siempre se puede mejorar y para eso se aprende.


## Feedback
LLegue a ser uno de los seleccionados o no, me gustaria poder tener feedback de ustedes los desarrolladores por medio de gmail o por cualquier medio. Así podria seguir creciendo como profesional y aprender cada vezmas ya ue me gusta estar constantemente aprendiendo nuevas cosas. Les agradeceria si me pudieran dar feedback

## !Muchas Gracias por esta oportunidad!